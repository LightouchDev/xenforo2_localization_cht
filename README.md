# XenForo2 Mandarin Chinese Localization

### XenForo2 正體中文翻譯

# 工具要求
1. 使用 [OmegaT 3.6.0][2] 或以上（推薦使用 4.0 以上）
2. 使用 OmegaT 的 [okapi plugin][3]
3. Tag Processing 請設為 `(\{.*?\})|(<.*?>)`
4. 選項/檔案管理器 找 `XML files (Okapi - XML Stream Filter)` 點「選項」按鈕，載入 `okf_xmlstream@preserveAll.fprm`
![](omegat-xml-setting.jpg)

請自行 fork repo 到自己的帳號下，並將你的 commit 發 pull request 給我。

以下規定引用自 [《塵埃：幸福的尾巴》公開中文化][4]
# 標點符號準則
符號一律使用全形符號，請參考[標點符號手冊][5]，打不出來的符號，就直接複製此網頁上的符號，不要找相似的貼，有時樣子一樣但編碼不一樣，套上字型後的呈現就會不同。

* 夾注號一律採用甲式。
* 書名號使用乙式第一種。
* 英文的單引號'、雙引號"一律改用中文引號「」，『』視情況使用。
* 英文的破折號-，中文是──，是兩個全形字，不是一個。
* 刪節號……也是兩個全形，不是英文的六個點......。
* 英文和中文之間不需留空白，符號跟英文和中文之間也不需留空白。
* 遇到特殊變數(遊戲中會取代成圖案、標誌之類的)，變數前後要留空白，鄰接標點符號則不用。
* 遇到 1 of 5 這種顯示數量的地方，可以用 1/5，/是英文半形斜線，不需要用全形的。
* 其它待補充。

[2]: https://sourceforge.net/projects/omegat/files/OmegaT%20-%20Standard/
[3]: http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT
[4]: https://github.com/codebayin/Dust.An.Elysian.Tail.zh_TW/blob/master/README.md
[5]: http://language.moe.gov.tw/001/Upload/FILES/SITE_CONTENT/M0001/HAU/haushou.htm
